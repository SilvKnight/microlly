La première page d'index est pour le visiteur. Elle affiche toutes les publications.
On peut accéder via cette page à la page de connexion et à la page d'inscription.
    
La page d'inscription crée un utilisateur dans la base de données.

La page de connexion, si la connexion s'est bien établie, mène vers la page d'index pour les utilisateurs : logged.html.

La page logged affiche les publications de l'utilisateur connecté. On peut via cette page accéder à la page de création de publications et à la page d'édition.
On peut aussi se déconnecter via cette page. Il est censé être afficher le nom de l'utilisateur courant à gauche du lien de déconnexion mais il affiche à la place
le premier utilsateur dans la base.

La page de création ajoute une publication dans la base de données.

La page d'édition modifie la publication. Le bouton Delete ne fonctionne pas.