from flask import Flask, url_for, render_template, request, flash, redirect
from models import *
from forms import *
from flask_login import LoginManager, login_user, login_required, logout_user, current_user


login_manager = LoginManager()


app = Flask(__name__)

login_manager.init_app(app)
login_manager.login_view = 'login'

@login_manager.user_loader
def user_load(user_id):
    return User.get(user_id)

app.secret_key = 'HelloWorld'

"""L'index visible par le visiteur comportant toutes les publications"""
@app.route('/', methods=['GET'])
def index():
    post = Publication.select()
    return render_template('index.html', post=post)


"""L'index visible par les utilisateurs comportant leurs publications"""
@app.route('/logged', methods=['GET'])
@login_required
def logged():
    post = Publication.select().where(Publication.user == current_user.id)
    return render_template('indexlog.html', post=post)

"""La page d'inscription"""
@app.route('/register', methods=['GET', 'POST', ])
def register():
    user = User()
    form = RegisterUserForm()
    if request.method == 'POST':
        form = RegisterUserForm(request.form)
        if form.validate():
            form.populate_obj(user)
            user.save()
            flash('User created !')
    return render_template('register.html', form=form)

"""La page pour se connecter"""
@app.route('/login', methods=['GET', 'POST', ])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.get_or_none(
            User.username == form.username.data, User.password == form.password.data)
        if user:
            login_user(user)

            """Affichage de l'utilisateur courant avec redirection vers l'index pour les utilisateurs"""
            flash('Welcome back ' + current_user.username)
            return redirect(url_for('logged'))
        else:
            flash('Invalid credential')
    return render_template('login.html', form=form)


"""Page de déconnexion"""
@app.route('/logout', methods=['GET'])
@login_required
def logout():
    logout_user()
    flash('You have been disconnected')
    return redirect(url_for('index'))


"""Création de publication"""
@app.route('/newpost', methods=['GET', 'POST', ])
@login_required
def newpost():
    post = Publication()
    form = PublicationForm()
    if request.method == 'POST':
        form = PublicationForm(request.form)
        if form.validate():
            form.populate_obj(post)
            post.user = current_user.id
            post.save()
            flash('Publication posted !')
    return render_template('newpost.html', form=form)


"""Edition de publication selon son id dans la base"""
@app.route('/editpost/<int:post_id>', methods=['GET', 'POST', ])
@login_required
def editpost(post_id):
    post = Publication.get(id=post_id)
    if request.method == 'POST':
        form = PublicationForm(request.form, obj=post)
        if form.validate():
            form.populate_obj(post)
            post.save()
            flash('Your entry has been saved')
    else:
        form = PublicationForm(obj=post)
    return render_template('editpost.html', form=form, post=post)


@app.cli.command()
def initdb():
    """Create database"""
    create_tables()
    click.echo('Initialized the database')


@app.cli.command()
def dropdb():
    """Drop database tables"""
    drop_tables()
    click.echo('Dropped tables from database')
