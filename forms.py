
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, PasswordField, TextAreaField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length

from wtfpeewee.orm import model_form
from models import *


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[
        DataRequired(), Length(min=3, max=20)])
    password = PasswordField('Password', validators=[
        DataRequired()])


class RegisterUserForm(FlaskForm):
    username = StringField('Username', validators=[
                       DataRequired(), Length(min=3, max=20)])
    first_name = StringField('FirstName', validators=[
                       DataRequired(), Length(min=3, max=20)])
    last_name = StringField('LastName', validators=[
                       DataRequired(), Length(min=3, max=20)])
    email = StringField('Email', validators=[
                       DataRequired(), Length(min=3, max=50)])
    password = PasswordField('Password', validators=[
                        DataRequired()])

class PublicationForm(FlaskForm):
    title = StringField('Title', validators=[
                       DataRequired(), Length(min=3, max=20)])
    body = TextAreaField('Body', validators=[
                       DataRequired()])

